﻿var webpack = require('webpack');
var webpackConfig = require("./config");
var koa_webpack_middleware = require('koa-webpack-middleware')
var devMiddleware=koa_webpack_middleware.devMiddleware;
var hotMiddleware=koa_webpack_middleware.hotMiddleware;
var compile = webpack(webpackConfig("dev"));
var app = require('./koa-server/index.js').app;

app.use(devMiddleware(compile, {
	stats: {
		colors: true
	}
}));

app.use(hotMiddleware(compile,{
	// log: console.log,
	// path: '/__webpack_hmr',
	// heartbeat: 10 * 1000
}))
