var path = require('path');
var publicPath="/";
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = function() {
	return {
		entry:{"bundle":'./src/entry.js'},
		output: {
			publicPath:publicPath,
			filename: 'index_bundle.js',
			path: path.resolve(__dirname, 'dist'),
			pathinfo:true
		},
		resolve: {
			extensions: ['.js', '.json'],
			modules: [path.join(__dirname, 'src'), 'node_modules']
		},
		module: {
			rules: [{
				test: /\.css$/,
				use: ['to-string-loader', 'css-loader']
			}, {
				test: /\.(jpg|png|gif)$/,
				use: 'file-loader'
			}, {
				test: /\.(woff|woff2|eot|ttf|svg)$/,
				use: {
					loader: 'url-loader',
					options: {
						limit: 100000
					}
				}
			},
				{
					test: /\.(js)$/,
					use: 'babel-loader',
					exclude: /(node_modules|bower_components)/
				}
			]
		},
		plugins: [
			new HtmlWebpackPlugin(),
			new HtmlWebpackPlugin({
				template: 'src/index.html',
				chunks: ['bundle'],
				filename:'index.html',
				inject:true
			}),
			new webpack.optimize.OccurrenceOrderPlugin(),
			new webpack.HotModuleReplacementPlugin(),
			/*
			new webpack.NamedModulesPlugin()
			*/
		]
	};
}
